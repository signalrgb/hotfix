import {Assert} from "@SignalRGB/Errors.js";

export function Name() { return "Asrock Motherboard"; }
export function VendorId() { return   0x26CE; }
export function ProductId() { return  0x01A2; }
export function Documentation(){ return "troubleshooting/asrock"; }
export function Publisher() { return "WhirlwindFX"; }
export function Size() { return [1, 1]; }
export function Type() { return "Hid"; }
export function DefaultPosition(){return [0, 0];}
export function DefaultScale(){return 1.0;}
export function DeviceType(){return "lightingcontroller";}

/* global
shutdownColor:readonly
LightingMode:readonly
forcedColor:readonly
Mainboardconfig:readonly
Headerconfig:readonly
RGBconfig:readonly
ARGBMode:readonly
overrideColor:readonly
*/
export function ControllableParameters() {
	return [
		{"property":"shutdownColor", "group":"lighting", "label":"Shutdown Color", description: "This color is applied to the device when the System, or SignalRGB is shutting down", "min":"0", "max":"360", "type":"color", "default":"#000000"},
		{"property":"LightingMode", "group":"lighting", "label":"Lighting Mode", description: "Determines where the device's RGB comes from. Canvas will pull from the active Effect, while Forced will override it to a specific color", "type":"combobox", "values":["Canvas", "Forced"], "default":"Canvas"},
		{"property":"forcedColor", "group":"lighting", "label":"Forced Color", description: "The color used when 'Forced' Lighting Mode is enabled", "min":"0", "max":"360", "type":"color", "default":"#009bde"},
		{"property":"Mainboardconfig", "group":"lighting", "label":"Mainboard Configuration", description: "Sets the RGB color order for the Motherboards RGB. If you are experiencing issues, try switching to each one of these options until you find one which works", "type":"combobox",   "values":["RGB", "RBG", "BGR", "BRG", "GBR", "GRB"], "default":"RGB"},
		{"property":"Headerconfig", "group":"lighting", "label":"12v Header Configuration", "type":"combobox", description: "Sets the RGB color order for the Motherboards 12v RGB Headers. If you are experiencing issues, try switching to each one of these options until you find one which works",  "values":["RGB", "RBG", "BGR", "BRG", "GBR", "GRB"], "default":"RGB"},
		{"property":"RGBconfig", "group":"lighting", "label":"ARGB Channel Configuration", description: "Sets the RGB color order for the Motherboards 5v ARGB Headers. If you are experiencing issues, try switching to each one of these options until you find one which works", "type":"combobox",   "values":["RGB", "RBG", "BGR", "BRG", "GBR", "GRB"], "default":"RGB"},
		{"property":"ARGBMode", "group":"", "label":"PerLED Canvas Support", description: "Enables Per-LED mode. If your board flickers with the toggle enabled, it means that your motherboard doesn't support Per-LED mode", "type":"boolean", "default": "true"},
	];
}

const vARGBLedNames = [ ];
/** @type {LedPosition[]} */
const vARGBLedPositions = [ ];

const vZoneLeds = [ "12v RGB 1", "12v RGB 2", "5v ARGB 1", "5v ARGB 2", "PCH", "IO Cover", "PCB", "5v ARGB 3" ];

const RGBConfigs = {
	"RGB" : [0, 1, 2],
	"RBG" : [0, 2, 1],
	"BGR" : [2, 1, 0],
	"BRG" : [2, 0, 1],
	"GBR" : [1, 2, 0],
	"GRB" : [1, 0, 2]
};

let modeChanged = false;

export function SubdeviceController(){ return true; }

export function LedNames() {
	return vARGBLedNames;
}

export function LedPositions() {
	return vARGBLedPositions;
}

export function Initialize() {
	device.clearReadBuffer();
	device.setName(`Asrock ${device.getMotherboardName()}`);

	createChannels();
}

export function Render() {
	if(!ARGBMode) {
		grabZones();
	} else {
		SendRGB();
	}
}

export function Shutdown(SystemSuspending) {
	const color = SystemSuspending ? "#000000" : shutdownColor;

	if(!ARGBMode) {
		grabZones(color);
	} else {
		SendRGB(color);
	}
}

export function onARGBModeChanged(){
	const subdevicesToRemove = device.getCurrentSubdevices();
	const channelsToRemove = device.getChannelNames();

	for(let i = 0; i < subdevicesToRemove.length; i++){
		device.removeSubdevice(subdevicesToRemove[i]);
	}

	for(let i = 0; i < channelsToRemove.length; i++){
		device.removeChannel(channelsToRemove[i]);
	}

	createChannels();
	modeChanged = true;
}

function createChannels(){
	if(!ARGBMode) {
		CreateZones();
	} else {
		LEDConfig();
		CreateARGBHeaders();
		CreateRGBHeaders();
		CreateIOShieldZone();
		CreatePCBZone();
		CreatePCHZone();
	}
}

function ReadConfig(ConfigId) {
	device.clearReadBuffer();
	device.pause(5);
	device.write([0x00, 0x14, 0x00, ConfigId], 65);
	device.pause(5);

	return device.read([0x00, 0x14, 0x00, ConfigId], 65);
}

function CreateZones() {
	device.write([0x00, 0x10, 0x00, 0x02, 0x0E, 0xff, 0xf2, 0x00, 0x6d, 0xff], 65);

	for(let leds = 0; leds < vZoneLeds.length; leds++) {
		device.createSubdevice(vZoneLeds[leds]);

		device.setSubdeviceName(vZoneLeds[leds], `${"Asrock RGB Controller"} - ${vZoneLeds[leds]}`);

		device.setSubdeviceSize(vZoneLeds[leds], 2, 2);

		device.setSubdeviceLeds(vZoneLeds[leds], [ vZoneLeds[leds] ], [ [0, 0] ]);
	}
}

function setColorOrder(col, led) {
	const ledColorArray = [];

	if(["12v RGB 1", "12v RGB 2"].includes(led)) {
		ledColorArray[0] = col[RGBConfigs[Headerconfig][0]];
		ledColorArray[1] = col[RGBConfigs[Headerconfig][1]];
		ledColorArray[2] = col[RGBConfigs[Headerconfig][2]];

		return ledColorArray;
	}

	if(["5v ARGB 1", "5v ARGB 2", "5v ARGB 3"].includes(led)) {
		ledColorArray[0] = col[RGBConfigs[RGBconfig][0]];
		ledColorArray[1] = col[RGBConfigs[RGBconfig][1]];
		ledColorArray[2] = col[RGBConfigs[RGBconfig][2]];

		return ledColorArray;
	}

	if(["PCH", "IO Cover", "PCB"].includes(led)) {
		ledColorArray[0] = col[RGBConfigs[Mainboardconfig][0]];
		ledColorArray[1] = col[RGBConfigs[Mainboardconfig][1]];
		ledColorArray[2] = col[RGBConfigs[Mainboardconfig][2]];

		return ledColorArray;
	}

	device.log("An LED is somewhere it shouldn't be!");

	return [0, 0, 0];
}

function grabZones(overrideColor) {
	if(modeChanged){
		modeChanged = false;

		return;
	}

	const RGBData = [];

	for(let leds = 0; leds < vZoneLeds.length; leds++) {
		let col;

		if(overrideColor) {
			col = hexToRgb(overrideColor);
		} else if (LightingMode === "Forced") {
			col = hexToRgb(forcedColor);
		} else {
			col = device.subdeviceColor(vZoneLeds[leds], 0, 0);
		}

		col = setColorOrder(col, vZoneLeds[leds]);

		const iLedIdx = (leds*3);
		RGBData[iLedIdx] = col[0];
		RGBData[iLedIdx+1] = col[1];
		RGBData[iLedIdx+2] = col[2];
	}

	device.write([0x00, 0x10, 0x00, 0x07, 0xE2].concat(RGBData), 65);
}

//ARGB SECTION ----------------------------------------------------------------------

const DeviceMaxLedLimit = 240;

//Channel Name, Led Limit
const ChannelArray =
[
	["Channel 1", 80],
	["Channel 2", 80],
	["Channel 3", 80]
];

const HeaderArray =
[
	"12v RGB Header 1",
	"12v RGB Header 2"
];

const ConfigurationOverrides = //Leave this here for now. Just in case
{
	"X570 Taichi Razer Edition":
    {
    	RGBHeader1  : 1,
    	RGBHeader2  : 1,
    	ARGBHeader1 : 80,
    	ARGBHeader2 : 80,
    	PCH         : 30,
    	IOShield    : 7,
    	PCB         : 12,
    	ARGBHeader3 : 0
    },
	"B650M Pro RS WiFi":
    {
    	RGBHeader1  : 1,
    	RGBHeader2  : 0,
    	ARGBHeader1 : 80,
    	ARGBHeader2 : 80,
    	PCH         : 0,
    	IOShield    : 0,
    	PCB         : 3,
    	ARGBHeader3 : 80
    },
	"B550M Steel Legend":
    {
    	RGBHeader1  : 1,
    	RGBHeader2  : 1,
    	ARGBHeader1 : 80,
    	ARGBHeader2 : 80,
    	PCH         : 5,
    	IOShield    : 7,
    	PCB         : 8,
    	ARGBHeader3 : 0
    },
	"X870E Nova WiFi":
    {
    	RGBHeader1  : 1,
    	RGBHeader2  : 0,
    	ARGBHeader1 : 80,
    	ARGBHeader2 : 80,
    	PCH         : 0,
    	IOShield    : 15,
    	PCB         : 10,
    	ARGBHeader3 : 80
    },

};

const deviceZones =
{
	"RGBHeader1"  : 0,
	"RGBHeader2"  : 0,
	"ARGBHeader1" : 0,
	"ARGBHeader2" : 0,
	"PCH"		  : 0,
	"IOShield"	  : 0,
	"PCB"		  : 0,
	"ARGBHeader3" : 0
};

function LEDConfig() {
	const zoneLEDCounts = ReadConfig(2).slice(5, 13);
	device.log(`Device LED Counts: ${zoneLEDCounts}`, {toFile : true});

	device.clearReadBuffer();
	device.pause(5);

	device.write([0x00, 0x14, 0x00, 0x01], 65);
	device.pause(5);

	const comparisonValue = device.read([0x00], 65)[5];

	if(device.getMotherboardName() in ConfigurationOverrides) {
		device.log(`Using magic of Tables for this mobo. MSI would be proud.`);

		const moboName = device.getMotherboardName();

		deviceZones["RGBHeader1"] = ConfigurationOverrides[moboName]["RGBHeader1"];
		deviceZones["RGBHeader2"] = ConfigurationOverrides[moboName]["RGBHeader2"];
		deviceZones["ARGBHeader1"] = ConfigurationOverrides[moboName]["ARGBHeader1"];
		deviceZones["ARGBHeader2"] = ConfigurationOverrides[moboName]["ARGBHeader2"];
		deviceZones["PCH"] = ConfigurationOverrides[moboName]["PCH"];
		deviceZones["IOShield"] = ConfigurationOverrides[moboName]["IOShield"];
		deviceZones["PCB"] = ConfigurationOverrides[moboName]["PCB"];
		deviceZones["ARGBHeader3"] = ConfigurationOverrides[moboName]["ARGBHeader3"];

		return;
	}

	const zoneNames = Object.keys(deviceZones);

	for(let zone = 0; zone < 8; zone++) {
		const disabledZone = isZoneDisabled(comparisonValue, zone);

		if(!disabledZone) {
			if(zoneLEDCounts[zone] !== 30) {
				deviceZones[zoneNames[zone]] = zoneLEDCounts[zone];
				device.log(`Zone ${zoneNames[zone]} has ${zoneLEDCounts[zone]} LEDs.`, { toFile : true });
			}
		}
	}
}

function isZoneDisabled(value, bitIndex){
	return !((value >> bitIndex) & 1);
}

function SetupChannels(deviceChannels) {
	device.SetLedLimit(DeviceMaxLedLimit);

	for(let i = 0; i < device.getChannelNames().length; i++){
		device.removeChannel(ChannelArray[i][0], ChannelArray[i][1]);
	}

	for(let i = 0; i < deviceChannels; i++){
		device.addChannel(ChannelArray[i][0], ChannelArray[i][1]);
	}
}

let RGBHeaders = 0;
let ARGBHeaders = 0;

function CreateARGBHeaders() {
	ARGBHeaders = 0; //Let's not have 20 ARGB Headers lol.

	if(deviceZones["ARGBHeader1"] !== 0 && deviceZones["ARGBHeader1"] !== 30) {
		ARGBHeaders++;
	}

	if(deviceZones["ARGBHeader2"] !== 0 && deviceZones["ARGBHeader2"] !== 30) {
		ARGBHeaders++;
	}

	if(deviceZones["ARGBHeader3"] !== 0 && deviceZones["ARGBHeader3"] !== 30) {
		ARGBHeaders++;
	}

	SetupChannels(ARGBHeaders);
}

let pcbLedInfo = [];
let ioShieldLedInfo = [];
let pchLedInfo = [];

function CreateRGBHeaders() {
	RGBHeaders = 0;

	for(let header = 0; header < 2;header++) {
		device.removeSubdevice(HeaderArray[header]);
	}

	if(deviceZones["RGBHeader1"] === 1) {
		device.createSubdevice(HeaderArray[0]);

		device.setSubdeviceName(HeaderArray[0], `${"Asrock RGB Controller"} - ${HeaderArray[0]}`);

		device.setSubdeviceLeds(HeaderArray[0], [ "Header" ], [ [1, 1] ]);

		device.setSubdeviceSize(HeaderArray[0], 3, 3);
		RGBHeaders = 1;
	}

	if(deviceZones["RGBHeader2"] === 1) {
		device.createSubdevice(HeaderArray[1]);

		device.setSubdeviceName(HeaderArray[1], `${"Asrock RGB Controller"} - ${HeaderArray[1]}`);

		device.setSubdeviceLeds(HeaderArray[1], [ "Header" ], [ [1, 1] ]);

		device.setSubdeviceSize(HeaderArray[1], 3, 3);
		RGBHeaders = 2;
	}

}

function CreatePCHZone() {
	device.removeSubdevice("PCH");

	if(deviceZones["PCH"] > 0) {
		pchLedInfo = generateLeds(deviceZones["PCH"], 'Vertical');

		device.createSubdevice("PCH");

		device.setSubdeviceName("PCH", `${"Asrock RGB Controller"} - ${"PCH"}`);

		device.setSubdeviceSize("PCH", 3, deviceZones["PCH"]);

		device.setSubdeviceLeds("PCH", pchLedInfo.vLedNames, pchLedInfo.vLedPositions);
	}
}

function CreateIOShieldZone() {
	device.removeSubdevice("IO Shield");

	if(deviceZones["IOShield"] > 0) {
		ioShieldLedInfo = generateLeds(deviceZones["IOShield"], 'Vertical');

		device.createSubdevice("IO Shield");

		device.setSubdeviceName("IO Shield", `${"Asrock RGB Controller"} - ${"IO Shield"}`);

		device.setSubdeviceSize("IO Shield", 3, deviceZones["IOShield"]);

		device.setSubdeviceLeds("IO Shield", ioShieldLedInfo.vLedNames, ioShieldLedInfo.vLedPositions);
	}
}

function CreatePCBZone() {
	device.removeSubdevice("PCB");

	if(deviceZones["PCB"] > 0) {
		pcbLedInfo = generateLeds(deviceZones["PCB"], 'Horizontal');

		device.createSubdevice("PCB");

		device.setSubdeviceName("PCB", `${"Asrock RGB Controller"} - ${"PCB"}`);

		device.setSubdeviceSize("PCB", deviceZones["PCB"], 3);

		device.setSubdeviceLeds("PCB", pcbLedInfo.vLedNames, pcbLedInfo.vLedPositions);
	}
}

function generateLeds(count, orientation = 'Horizontal') {
	const ledInfo = {
		vLedNames : [],
		vLedPositions : []
	};

	for(let iIdx = 0; iIdx < count; iIdx++) {
		ledInfo.vLedNames.push(`LED ${iIdx + 1}`);
		ledInfo.vLedPositions.push(orientation === 'Horizontal' ? [iIdx, 0] : [0, iIdx]);
	}

	return ledInfo;
}

function grabRGBHeaderData(overrideColor) {
	const RGBHeaderData = [];

	for(let iIdx = 0; iIdx < RGBHeaders; iIdx++) {
		let col;

		if(overrideColor) {
			col = hexToRgb(overrideColor);
		} else if (LightingMode === "Forced") {
			col = hexToRgb(forcedColor);
		} else {
			col = device.subdeviceColor(HeaderArray[iIdx], 2, 2);
		}
		const iLedIdx = (iIdx*3);
		RGBHeaderData[iLedIdx] = col[RGBConfigs[Headerconfig][0]];
		RGBHeaderData[iLedIdx+1] = col[RGBConfigs[Headerconfig][1]];
		RGBHeaderData[iLedIdx+2] = col[RGBConfigs[Headerconfig][2]];
	}

	return RGBHeaderData;
}

function grabMoboData(overrideColor) {
	const MoboRGBData = [];

	const PCHData = grabSubdeviceData("PCH", deviceZones["PCH"], Mainboardconfig, overrideColor);
	const IOShieldData = grabSubdeviceData("IO Shield", deviceZones["IOShield"], Mainboardconfig, overrideColor);
	const PCBData =grabSubdeviceData("PCB", deviceZones["PCB"], Mainboardconfig, overrideColor);

	MoboRGBData.push(...PCHData);
	MoboRGBData.push(...IOShieldData);
	MoboRGBData.push(...PCBData);

	return MoboRGBData;
}

function getZoneLed(zone, index){
	switch (zone) {
	  case "PCH":
		return pchLedInfo.vLedPositions[index];
	  case "IO Shield":
		return ioShieldLedInfo.vLedPositions[index];
	  case "PCB":
		return pcbLedInfo.vLedPositions[index];
	  default:
		Assert.unreachable(`Zone ${zone} doesn't exist!`);
	}
}

function grabSubdeviceData(zone, ledCount, RGBConfig, overrideColor) {
	const RGBData	= [];

	for(let iIdx = 0; iIdx < ledCount; iIdx++) {
		const position = getZoneLed(zone, iIdx);
		Assert.isOk(position, "Expected valid Led Position", {zone, iIdx});

		let col;

		if(overrideColor) {
			col = hexToRgb(overrideColor);
		} else if (LightingMode === "Forced") {
			col = hexToRgb(forcedColor);
		} else {
			col = device.subdeviceColor(zone, position[0], position[1]);
		}
		const iLedIdx = (iIdx*3);
		RGBData[iLedIdx] = col[RGBConfigs[RGBConfig][0]];;
		RGBData[iLedIdx+1] = col[RGBConfigs[RGBConfig][1]];;
		RGBData[iLedIdx+2] = col[RGBConfigs[RGBConfig][2]];;
	}

	return RGBData;
}

function grabRGBData(Channel, overrideColor) {
	let ChannelLedCount = device.channel(ChannelArray[Channel][0]).LedCount();
	const componentChannel = device.channel(ChannelArray[Channel][0]);
	let RGBData = [];

	if (overrideColor) {
		RGBData = device.createColorArray(overrideColor, ChannelLedCount, "Inline", RGBconfig);

	} else if(LightingMode === "Forced") {
		RGBData = device.createColorArray(forcedColor, ChannelLedCount, "Inline", RGBconfig);

	} else if(componentChannel.shouldPulseColors()) {
		ChannelLedCount = 80;

		const pulseColor = device.getChannelPulseColor(ChannelArray[Channel][0]);
		RGBData = device.createColorArray(pulseColor, ChannelLedCount, "Inline", RGBconfig);

	} else {
		RGBData = device.channel(ChannelArray[Channel][0]).getColors("Inline", RGBconfig);
	}

	return RGBData.concat(new Array(240 - RGBData.length).fill(0));
}

function concatRGBData(overrideColor) {
	let RGBData = [];
	const MoboRGBData = grabMoboData(overrideColor);
	const RGBHeaderData = grabRGBHeaderData(overrideColor);
	const Header1RGBData = grabRGBData(0, overrideColor);


	//Properly Order Everything. RGBHeaders, Headers 1 and 2, Mobo, Header 3.

	RGBData = RGBHeaderData;
	RGBData = RGBData.concat(Header1RGBData);

	if(deviceZones["ARGBHeader2"] === 80) {
		const Header2RGBData = grabRGBData(1, overrideColor);
		RGBData = RGBData.concat(Header2RGBData);
	} else {
		const Header2RGBData = new Array(240);
		RGBData = RGBData.concat(Header2RGBData);
	}

	RGBData = RGBData.concat(MoboRGBData);

	if(deviceZones["ARGBHeader3"] === 80) {
		const header3RGBData = grabRGBData(2, overrideColor);
		RGBData = RGBData.concat(header3RGBData);
	}

	return RGBData;
}

function SendRGB(overrideColor) {
	if(modeChanged){
		modeChanged = false;

		return;
	}

	// packet[64] = 0xf0; //this is seemingly arbitrary. No idea what it does, did not change depending on the data in the packet. Every packet had it. B550 had 0x64 or 0x65, and the Z690 was 0xf0.
	const RGBData = concatRGBData(overrideColor);

	//const initpacket = [0x00, 0x10, 0x00, 0xff, 0xE3, 0x00, 0x00, (TotalDeviceLEDs & 0xff), (TotalDeviceLEDs >> 8 & 0xff)];

	device.write([0x00, 0x10, 0x00, 0xff, 0xE3, 0x00, 0x00, 0x2f, 0x01].concat(RGBData.splice(0, 54)), 65);

	for(let packetsSent = 0; packetsSent < 15; packetsSent++) //I'm still not a fan of hardcoding things, but this works on all of the boards.
	{
		device.write([0x00, 0x10, 0x00, 0xff, 0xE4].concat(RGBData.splice(0, 57)), 65);
	}
}

function hexToRgb(hex) {
	const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	const colors = [];
	colors[0] = parseInt(result[1], 16);
	colors[1] = parseInt(result[2], 16);
	colors[2] = parseInt(result[3], 16);

	return colors;
}

export function Validate(endpoint) {
	return endpoint.interface === 0;
}

export function ImageUrl() {
	return "https://assets.signalrgb.com/devices/brands/asrock/motherboards/motherboard.png";
}
