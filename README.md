# Hotfix

[![Add To Installation](https://marketplace.signalrgb.com/resources/add-extension-256.png 'Add to My SignalRGB Installation')](signalrgb://addon/install?url=https://gitlab.com/signalrgb/hotfix)

## Getting started
This is a simple SignalRGB Addon to fix bugs in SignalRGB Plugins in between patches.

## Installation
Click the button above and allow signalrgb to install this extension when prompted.